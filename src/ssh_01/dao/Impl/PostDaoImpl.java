package ssh_01.dao.Impl;

import java.util.List;

// import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.Transaction;

import ssh_01.entity.Post;
import ssh_01.dao.PostDao;

public class PostDaoImpl implements PostDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void createPost(Post post) {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(post);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public List<Post> readPost() {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();

		// HQL query
		String hql = "from Post";
		Query<Post> query = session.createQuery(hql, Post.class);

		// execute query, then put the result in List
		List<Post> postlist = query.getResultList();

		return postlist;
	}

	@Override
	public void updatePost(Post post) {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(post);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public void deletePost(Post post) {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(post);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
