package ssh_01.dao.Impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.Transaction;

import ssh_01.entity.Files;
import ssh_01.dao.FileDao;

public class FileDaoImpl implements FileDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void saveFile(Files files) {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(files);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public List<Files> readFile() {
		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();
		// get session
		Session session = sessionFactory.openSession();

		// HQL query
		String hql = "from Files";
		Query<Files> query = session.createQuery(hql, Files.class);

		// execute query, then put the result in List
		List<Files> filelist = query.getResultList();

		return filelist;
	}

}
