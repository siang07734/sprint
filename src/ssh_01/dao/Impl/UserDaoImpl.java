package ssh_01.dao.Impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import ssh_01.dao.UserDao;
import ssh_01.entity.User;

public class UserDaoImpl implements UserDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public User findUser(User user) {
		System.out.println("/--user_dao--/");
		
		User firstuser = new User();

		// create session
		Configuration cfg = new Configuration().configure();
		sessionFactory = cfg.buildSessionFactory();

		// get session
		Session session = sessionFactory.openSession();
		
		// HQL query
		String hql = "from User user where user.username='" + user.getUsername() + "' and user.password= '"
				+ user.getPassword() + "'";
		Query<User> query = session.createQuery(hql, User.class);

		// execute query, then put the result in List
		List<User> userlist = query.getResultList();
		
		if (userlist.size() > 0) {
			firstuser = userlist.get(0);
		}
		return firstuser;
	}
}
