package ssh_01.dao;
import ssh_01.entity.User;

public interface UserDao {
	public User findUser(User user);
}
