package ssh_01.dao;

import java.util.List;

import ssh_01.entity.Files;

public interface FileDao {
	public void saveFile(Files files);

	public List<Files> readFile();		

}
