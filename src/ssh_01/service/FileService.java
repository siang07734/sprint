package ssh_01.service;

import java.util.List;

import ssh_01.entity.Files;

public interface FileService {
	public void saveFile(Files files);
	public List<Files> readFile();
}
