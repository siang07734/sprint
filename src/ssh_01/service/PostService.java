package ssh_01.service;

import java.util.List;

import ssh_01.entity.Post;

public interface PostService {
	
	public void createPost(Post post);
	public List<Post> readPost();
	public void updatePost(Post post);
	public void deletePost(Post post);
	
}
