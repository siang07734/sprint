package ssh_01.service.Impl;

import java.util.List;

import ssh_01.dao.FileDao;
import ssh_01.entity.Files;
import ssh_01.service.FileService;

public class FileServiceImpl implements FileService {
	private FileDao filedao;

	public FileDao getFiledao() {
		return filedao;
	}

	public void setFiledao(FileDao filedao) {
		this.filedao = filedao;
	}

	@Override
	public void saveFile(Files files) {
		this.filedao.saveFile(files);
	}

	@Override
	public List<Files> readFile() {
		List<Files> filelist = this.filedao.readFile();
		return filelist;
	}
}
