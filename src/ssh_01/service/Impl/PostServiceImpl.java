package ssh_01.service.Impl;

import java.util.Date;
import java.util.List;

import ssh_01.entity.Post;
import ssh_01.service.PostService;
import ssh_01.dao.PostDao;

public class PostServiceImpl implements PostService {
	private PostDao postdao;

	public PostDao getPostdao() {
		return postdao;
	}

	public void setPostdao(PostDao postdao) {
		this.postdao = postdao;
	}

	@Override
	public void createPost(Post post) {
		post.setCreate_date(new Date());
		//post.setUpdate_date(new Date());
		this.postdao.createPost(post);
	}

	@Override
	public List<Post> readPost() {
		List<Post> postlist = this.postdao.readPost();
		return postlist;
	}

	@Override
	public void updatePost(Post post) {
		post.setUpdate_date(new Date());
		this.postdao.updatePost(post);

	}

	@Override
	public void deletePost(Post post) {
		this.postdao.deletePost(post);
	}

}
