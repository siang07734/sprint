package ssh_01.service.Impl;

import ssh_01.entity.User;
import ssh_01.service.UserService;
import ssh_01.dao.UserDao;

public class UserServiceImpl implements UserService {

	private UserDao userdao;

	public UserDao getUserdao() {
		return userdao;
	}

	public void setUserdao(UserDao userdao) {
		this.userdao = userdao;
	}

	@Override
	public boolean findUser(User user) {
		System.out.println("/--user_service--/");

		User firstuser = this.userdao.findUser(user);

		if (firstuser.getUsername() != null) {
			return true;
		} else {
			return false;
		}
	}
}