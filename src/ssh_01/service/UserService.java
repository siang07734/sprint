package ssh_01.service;
 
import ssh_01.entity.User;
 
public interface UserService {
	
	public boolean findUser(User user);
}