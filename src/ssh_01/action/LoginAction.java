package ssh_01.action;

import java.util.Map;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

import ssh_01.entity.User;
import ssh_01.service.UserService;

public class LoginAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private User user = new User();
	private UserService userservice;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserService getUserservice() {
		return userservice;
	}

	public void setUserservice(UserService userservice) {

		this.userservice = userservice;
	}

	@Override
	public String execute() throws Exception {
		// 建立 session 連線
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> session = actionContext.getSession();
		// 讀取使用者的 LOGIN_STATUS，如果已經登入過且 session 未過期，則直接登入。
		String loginStatus = (String) session.get("LOGIN_STATUS");
		if (loginStatus != null) {
			return SUCCESS;
		}

		boolean flag = userservice.findUser(user);
		if (flag) {
			session.put("USER_NAME", user.getUsername());
			session.put("LOGIN_STATUS", "success");
			return SUCCESS;
		} else {
			return INPUT;
		}

	}
}