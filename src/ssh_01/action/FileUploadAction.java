package ssh_01.action;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.File;
import org.apache.commons.io.FileUtils;
import java.io.IOException;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class FileUploadAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// file upload
	private List<File> newFile = new ArrayList<File>();
	private List<String> newFileContentType = new ArrayList<String>();
	private List<String> newFileFileName = new ArrayList<String>();
	private String destPath;

	public List<File> getNewFile() {
		return newFile;
	}

	public void setNewFile(List<File> newFile) {
		this.newFile = newFile;
	}

	public List<String> getNewFileFileName() {
		return newFileFileName;
	}

	public void setNewFileFileName(List<String> newFileFileName) {
		this.newFileFileName = newFileFileName;
	}

	public List<String> getNewFileContentType() {
		return newFileContentType;
	}

	public void setNewFileContentType(List<String> newFileContentType) {
		this.newFileContentType = newFileContentType;
	}

	public String execute() throws Exception {
		/* Copy file to a safe location */
		// destPath = "C:/apache-tomcat-9.0.21/work/upload/";
		destPath = "C:/eclipse-workspace/sprint/WebContent/static/upload/";
		// 新增用於回傳 JSON 結果的物件陣列
		List<Map> JsonResponse = new ArrayList<Map>();
		try {

			for (int i = 0, size = newFile.size(); i < size; i++) {
				File file = newFile.get(i);
				String filename = newFileFileName.get(i);
				System.out.println("file: " + file);
				System.out.println("filename: " + filename);
				File destFile = new File(destPath, filename);
				FileUtils.copyFile(file, destFile);

				Map<String, String> map = new HashMap<String, String>();
				map.put("file_name", filename);
				map.put("file_location", "static/upload/" + filename);
				JsonResponse.add(map);
			}
			ActionContext.getContext().getValueStack().push(JsonResponse);
		} catch (IOException e) {
			e.printStackTrace();
			ActionContext.getContext().getValueStack().push("error.");
		}

		return SUCCESS;
	}

}