package ssh_01.action;

import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
// import com.opensymphony.xwork2.ActionContext;

import ssh_01.entity.Post;
import ssh_01.service.PostService;

public class PostAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Post post = new Post();
	private PostService postservice;
	private InputStream inputStream;

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public PostService getPostservice() {
		return postservice;
	}

	public void setPostservice(PostService postservice) {
		this.postservice = postservice;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String create() throws Exception {
		this.postservice.createPost(post);
		inputStream = new ByteArrayInputStream("create success.".getBytes("UTF-8"));
		return SUCCESS;
	}

	public String read() throws Exception {
		List<Post> postList = this.postservice.readPost();
		ActionContext.getContext().getValueStack().push(postList);
		return SUCCESS;
	}

	public String update() throws Exception {
		this.postservice.updatePost(post);
		inputStream = new ByteArrayInputStream("update success.".getBytes("UTF-8"));
		return SUCCESS;
	}

	public String delete() throws Exception {
		this.postservice.deletePost(post);
		inputStream = new ByteArrayInputStream("delete success.".getBytes("UTF-8"));
		return SUCCESS;
	}
	
	public String file() throws Exception {
		return SUCCESS;
	}

	@Override
	public String execute() throws Exception {
		return SUCCESS;

	}
}
