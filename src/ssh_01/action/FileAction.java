package ssh_01.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import ssh_01.entity.Files;
import ssh_01.service.FileService;

public class FileAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// file save
	private Files files = new Files();
	private FileService fileservice;

	public Files getFiles() {
		return files;
	}

	public void setFile(Files files) {
		this.files = files;
	}
	
	public FileService getFileservice() {
		return fileservice;
	}

	public void setFileservice(FileService fileservice) {
		this.fileservice = fileservice;
	}

	public String create() throws Exception {
		this.fileservice.saveFile(files);
		ActionContext.getContext().getValueStack().push("success");
		return SUCCESS;
	}

	public String read() throws Exception {
		List<Files> fileList = this.fileservice.readFile();
		ActionContext.getContext().getValueStack().push(fileList);
		return SUCCESS;
	}

}