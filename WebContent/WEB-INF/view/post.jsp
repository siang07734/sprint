<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%
	if (!session.isNew()){
		 String loginStatus = (String)session.getAttribute("LOGIN_STATUS");
		if(!"success".equals(loginStatus)) {
			response.sendRedirect(basePath);
		}
	} 
%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Post System</title>
	<link rel="stylesheet" href="<%=basePath%>static/plugins/bootstrap.css">
	<link rel="stylesheet" href="<%=basePath%>static/style.css">

	<script src="<%=basePath%>static/plugins/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/plugins/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<!-- Jquery serializejson -->
	<script src="<%=basePath%>static/plugins/jquery-serializeJSON/jquery.serializejson.min.js" type="text/javascript"
		charset="utf-8"></script>
	<script src="<%=basePath%>static/plugins/ajaxfileupload.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/script.js" type="text/javascript" charset="utf-8"></script>

</head>

<body id="post">
	<div class="post">
		<!-- header-area -->
		<div class="post-header container-fluid">
			<div class="container">
				<div class="row">
					<!-- username -->
					<div class="col-xs-6 text-left">
						<label>
							Hi,
							<s:property value="#session.USER_NAME" />
						</label>
					</div>
					<div class="col-xs-6 text-right">
						<!-- use struts form -->
						<s:form action="/logout" method="post" class="form-horizontal">
							<input type="submit" class="btn" value="Logout" />
						</s:form>
					</div>
				</div>
			</div>
		</div>
		<!-- content-area -->
		<div class="post-body container">
			<!-- main-content -->
			<div class="post-content row">

			</div>
			<hr>
			<!-- post-form -->
			<div class="post-form">
				<div class="editor">
					<div class="editor-content">
						<s:form id="create_form" class="form-horizontal">
							<!-- public -->
							<input type="text" id="editor-content" name="post.content" class="form-control"
								placeholder="text something...">
							<!-- hidden -->
							<input type="hidden" name="post.username" value="<s:property value="
								#session.USER_NAME" />">
						</s:form>
					</div>
					<div class="editor-file"></div>
					<div class="editor-footer text-right">
						<button id="upload" class="btn btn-warning" data-id="new">Add File</button>
						<button id="create" class="btn btn-primary">Send</button>
					</div>
					<!-- upload-temp, this area is hidden. -->
					<div id="upload-temp" class="hidden">
						<form method="POST" enctype="multipart/form-data">
							<input type="file" name="newFile" multiple="multiple" id="newFile" class="file-upload"
								style="display:none;" />
						</form>
						<span id="file_temp" data-id="0"></span>
						<div id="max_post" data-max="0"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		const sshUrl = "<%=basePath%>";
		const username = '<s:property value="#session.USER_NAME" />';
		$(function () {
			ajax_read();
		});

		// 展開 collapse("修改" 與 "刪除" 區塊) 後的行為
		$(document).on('click', '.collapsebutton', function () {
			if (!$(this).find('.in').attr("data-in")) {
				$('.widget-body>.collapse').collapse('hide') // 先關閉所有清單
				$(this).find('.collapse').collapse('show') // 再展開目標清單
			}
		})
		// 關閉 collapse 後的行為
		$('.collapsebutton').on('hidden.bs.collapse', function () {
			let post_id = $(this).attr("data-id");
			let target = '#post_' + post_id;
			$(target).find('form').addClass("enabled");
			$(target).find('#user_content').attr('disabled', true);
			$(target).find('#update').hide();
			$(target).find('#upload').hide();
			$(target).find('#edit').show();

		})
		// 設定點擊 "edit" 後的行為
		$(document).on('click', '#edit', function () {
			let post_id = $(this).attr("data-id");
			let target = '#post_' + post_id;
			$(target).find('form').removeClass("enabled");
			$(target).find('#user_content').attr('disabled', false);
			$(target).find('#update').show();
			$(target).find('#upload').show();
			$(target).find('#edit').hide();
		})
		// 設定點擊 "send/create" 後的行為
		$(document).on('click', '#create', function () {
			var data = $("#create_form").serializeJSON();
			ajax_create(data);
			$('#editor-content').val('');
			$('.editor-file').html('');
		})
		// 設定點擊 "update/save" 後的行為
		$(document).on('click', '#update', function () {
			let post_id = $(this).attr("data-id");
			let target = '#post_' + post_id;
			var data = $(target).find("form").serializeJSON();
			ajax_update(data);

		})

		// 設定點擊 "delete" 後的行為
		$(document).on('click', '#delete', function () {
			let post_id = $(this).attr("data-id");
			let target = '#post_' + post_id;
			var data = $(target).find("form").serializeJSON();
			ajax_delete(data);

		})

		function ajax_create(data) {
			var AjaxUrl = sshUrl + 'post/create';
			$.ajax({
				type: "post",
				url: AjaxUrl,
				data: {
					'post.username': username,
					'post.content': data['post.content']
				},
				success: function (response) {
					console.log(response);
					ajax_read();
				},
				error: function (error) {
					alert('[create] server error.')
				}
			});
		}

		function ajax_read() {
			var AjaxUrl = sshUrl + 'post/read';
			var collapsebutton = '';
			$.ajax({
				type: "post",
				url: AjaxUrl,
				dataType: 'json',
				success: function (response) {
					// console.log(response)
					var max_post = '';
					var content = '';
					response.forEach(e => {
						max_post = e.post_id;
						// 如果此留言來自目前登入的使用者，等等列印物件的時候才提供留言編輯功能
						if (e.username == username) collapsebutton = 'collapsebutton';
						// create content.
						// 01.header-area
						content = content +
							`<div id="post_` + e.post_id + `" class="widget-body ` + collapsebutton +
							`" data-id="` + e.post_id + `">` +
							`<div class="widget-header row">` +
							`<div class="header-name col-xs-4 text-left">` +
							`<label>` +
							e.username +
							`</label>` +
							`</div>` +
							`<div class="header-time col-xs-8 text-right">` +
							`<span>post ` + e.create_date + `</span>` +
							`<span>, </span>` +
							`<span>edited ` + e.update_date + `</span>` +
							`</div>` +
							`</div>`;
						// 02.content-area
						content = content +
							`<div class="widget-content">` +
							`<s:form class="form-horizontal enabled">` +
							`<input type="hidden" name="post.post_id" value="` + e.post_id + `">` +
							`<input type="hidden" name="post.username" value="` + e.username + `">` +

							`<input type="text" name="post.content" id="user_content" class="form-control" value="` +
							e.content + `" disabled="disabled">` +
							`<input type="hidden" name="post.create_date" value="` + e.create_date +
							`">` +
							`</s:form>` +
							`</div>`;
						// 03.file-area
						// if(e.file_location) {
						content = content +
							`<div class="widget-file">` +
							`</div>`
						// }
						// 04.footer-area
						content = content +
							`<div class="collapse" data-in="in" data-id="` + e.post_id + `">` +
							`<hr>` +
							`<div class="widget-footer text-right">` +
							`<button id="upload" class="btn btn-warning" data-id="` +
							e.post_id + `" style="display:none;">Add File</button>` +
							`<button id="update" class="btn btn-primary" data-id="` + e.post_id +
							`">Save</button>` +
							`<button id="edit" class="btn btn-info" data-id="` + e.post_id +
							`">Edit</button>` +
							`<button id="delete" class="btn btn-danger" data-id="` + e.post_id +
							`">Delete</button>` +
							`</div>` +
							`</div>` +
							`</div>`;
						collapsebutton = '';
					});
					$('.post-content').html(content)
					// 設置目前 post 的最大值
					$('#max_post').attr('data-max', parseInt(max_post) + 1);
					readFileInfo();
				},
				error: function (error) {
					alert('[read] server error.')
				}
			});
		}

		function ajax_update(data) {
			var AjaxUrl = sshUrl + 'post/update'
			$.ajax({
				type: "post",
				url: AjaxUrl,
				data: {
					'post.post_id': data['post.post_id'],
					'post.username': data['post.username'],
					'post.content': data['post.content'],
					'post.create_date': data['post.create_date']
				},
				success: function (response) {
					console.log(response)
					ajax_read();
				},
				error: function (error) {
					alert('[update] server error.')
				}
			});
		}

		function ajax_delete(data) {
			var AjaxUrl = sshUrl + 'post/delete'
			$.ajax({
				type: "post",
				url: AjaxUrl,
				data: {
					'post.post_id': data['post.post_id']
				},
				success: function (response) {
					console.log(response)
					ajax_read();
				},
				error: function (error) {
					alert('[delete] server error.')
				}
			});
		}

		// file upload
		// step.1 設定點擊 "upload" 後的行為，觸發檔案上傳
		$(document).on('click', '#upload', function () {
			// 取得觸發行為按鈕的 "post_id", 並暫存在 "#file_temp" 物件裡面
			const post_id = $(this).attr("data-id");
			$('#file_temp').attr('data-id', post_id)
			// 透過 js 觸發檔案上傳動作
			$("#newFile").click();
		})

		// step.2 檔案選擇完畢(使用者選擇檔案)後的動作
		$(document).on('change', '.file-upload', function () {
			let post_id = $("#file_temp").attr("data-id");
			ajax_fileUpload(post_id);
			// saveFileInfo(1, 1);

		})

		// step.3 透過 Ajax 上傳檔案
		function ajax_fileUpload(post_id) {
			var AjaxUrl = sshUrl + 'post/upload';
			$.ajaxFileUpload({
				url: AjaxUrl,
				secureuri: false, // 一般设置为 false
				fileElementId: 'newFile', // 文件上傳空間的id属性 <input type="file" id="file"/>
				dataType: 'json',
				success: function (response) {
					console.log('response :', response);
					saveFileInfo(response, post_id);
				},
				error: function (error) {
					console.log('error :', error);
				}
			})
		}

		// step.4 透過 Ajax 儲存檔案資訊
		function saveFileInfo(data, post_id) {
			// 如果是新留言，則給予目前留言編號的最大值 + 1
			var isNew = '';
			if (post_id == 'new') {
				post_id = $('#max_post').attr('data-max');
				isNew = 'new';
			}
			var requestdata = [];
			// 進行 Ajax 連線
			var AjaxUrl = sshUrl + 'file/create';
			data.forEach(e => {
				console.log('e :', e);
				$.ajax({
					type: "post",
					url: AjaxUrl,
					async: false,
					data: {
						'files.file_name': e['file_name'],
						'files.file_location': e['file_location'],
						'files.post_id': post_id
					},
					success: function (response) {
						setTimeout(console.log('file create success.'), 50000);
						// console.log("file create success.");
					},
					error: function (error) {
						alert('[create_file] server error.')
					}
				});

			});
	
			// setTimeout(ajax_read(), 50000);

		}

		// step.5 透過 Ajax 讀取檔案資訊
		function readFileInfo() {
			var AjaxUrl = sshUrl + 'file/read';
			$.ajax({
				type: "post",
				url: AjaxUrl,
				dataType: 'json',
				success: function (response) {
					console.log('response(readFileInfo) :', response);
					var content;
					response.forEach(e => {
						content =
							`<a href=" ` + sshUrl + e.file_location + `" download>` +
							e.file_name +
							`</a>`;
						$('#post_' + e.post_id).find('.widget-file').append(content);
					});
				},
				error: function (error) {
					alert('[read_file] server error.')
				}
			});
		}
	</script>
</body>

</html>