<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Error</title>
	<link rel="stylesheet" href="<%=basePath%>static/plugins/bootstrap.css">
	<link rel="stylesheet" href="<%=basePath%>static/style.css">

	<script src="<%=basePath%>static/plugins/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/plugins/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<!-- Jquery serializejson -->
	<script src="<%=basePath%>static/plugins/jquery-serializeJSON/jquery.serializejson.min.js" type="text/javascript"
		charset="utf-8"></script>
	<script src="<%=basePath%>static/script.js" type="text/javascript" charset="utf-8"></script>
</head>

<body id="error">
	<div class="container text-center">
		<h1>error.</h1>
		<h2>Incorrect username or password.</h2>
	</div>
</body>

</html>