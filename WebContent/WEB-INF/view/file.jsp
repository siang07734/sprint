<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>File Upload</title>
	<link rel="stylesheet" href="<%=basePath%>static/plugins/bootstrap.css">
	<link rel="stylesheet" href="<%=basePath%>static/style.css">

	<script src="<%=basePath%>static/plugins/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/plugins/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<!-- Jquery serializejson -->
	<script src="<%=basePath%>static/plugins/jquery-serializeJSON/jquery.serializejson.min.js" type="text/javascript"
		charset="utf-8"></script>
	<!-- Jquery ajaxfileupload -->
	<!-- <script src="<%=basePath%>static/plugins/jquery.ajaxfileupload.js" type="text/javascript" charset="utf-8"></script> -->
	<script src="<%=basePath%>static/plugins/ajaxfileupload.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/script.js" type="text/javascript" charset="utf-8"></script>
</head>

<body id="file">
	<div class="container text-center">
		<label class="btn btn-info">
			<input type="file" name="newFile" id="newFile" class="file-upload" style="display:none;" />
			Upload File
		</label>
	</div>

	<script type="text/javascript">
		// $('#myFile').ajaxfileupload({
		// 	action: '<%=basePath%>post/upload'
		// });
		$(".file-upload").change(function () {
			let obj_id = $(this).attr("id");
			console.log('obj_id :', obj_id);
			fileUpload(obj_id);
		});

		function fileUpload(obj_id) {
			$.ajaxFileUpload({
				url: '<%=basePath%>post/upload',
				secureuri: false, //一般设置为false
				fileElementId: obj_id, //	文件上傳空間的id属性  <input type="file" id="file" name="file" />
				dataType: 'json',
				success: function (data) {
					console.log('success');
					console.log('data :', data);
				},
				error: function (error) {
					console.log('error');
				}
			})
		}
	</script>
</body>

</html>