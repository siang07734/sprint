<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%
	if (!session.isNew()){
		 String loginStatus = (String)session.getAttribute("LOGIN_STATUS");
		if("success".equals(loginStatus)) {
			response.sendRedirect(basePath + "login");
		}
	} 
%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Post System｜Login</title>
	<link rel="stylesheet" href="<%=basePath%>static/plugins/bootstrap.css">
	<link rel="stylesheet" href="<%=basePath%>static/style.css">

	<script src="<%=basePath%>static/plugins/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/plugins/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<%=basePath%>static/script.js" type="text/javascript" charset="utf-8"></script>
</head>

<body id="login">
	<div class="container content">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
				<div class="card">
					<div class="card-image">
						<img src="<%=basePath%>static/image/user_2x.png" alt="user_2x" class="img-responsive img-circle">
					</div>
					<div class="card-form">
						<s:form action="/login" method="post" class="form-horizontal">
							<div class="form-group row">
								<div class="col-xs-12">
									<input type="text" name="user.username" class="form-control"
										placeholder="Username" />
								</div>
								<div class="col-xs-12">
									<input type="password" name="user.password" class="form-control"
										placeholder="Password" />
								</div>
								<div class="col-xs-12">
									<hr>
								</div>
								<div class="col-xs-12 text-center">
									<button type="submit" class="btn btn-block btn-primary">Sign in</button>
								</div>
							</div>
						</s:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>